
output "FGTActiveMGMTPublicIP" {
  value = aws_eip.MGMTPublicIP.public_ip
}

output "FGTClusterPublicFQDN" {
  value = join("", tolist(["https://", "${aws_eip.ClusterPublicIP.public_dns}", ":", "${var.adminsport}"]))
}

output "FGTClusterPublicIP" {
  value = aws_eip.ClusterPublicIP.public_ip
}


output "FGTPassiveMGMTPublicIP" {
  value = aws_eip.PassiveMGMTPublicIP.public_ip
}

output "Username" {
  value = "admin"
}

output "Password" {
  value = aws_instance.fgtactive.id
}

output "privatesubnetaz1" {
  value = aws_subnet.privatesubnetaz1.id
}

output "publicsubnetaz1" {
  value = aws_subnet.publicsubnetaz1.id
}

output "privatesubnetaz2" {
  value = aws_subnet.privatesubnetaz2.id
}

output "publicsubnetaz2" {
  value = aws_subnet.publicsubnetaz2.id
}

output "hamgmtsubnetaz1" {
  value = aws_subnet.hamgmtsubnetaz1.id
}

output "hasyncsubnetaz1" {
  value = aws_subnet.hasyncsubnetaz1.id
}

output "hamgmtsubnetaz2" {
  value = aws_subnet.hamgmtsubnetaz2.id
}

output "hasyncsubnetaz2" {
  value = aws_subnet.hasyncsubnetaz2.id
}

output "vpc_id" {
  value = aws_vpc.fgtvm-vpc.id
}
